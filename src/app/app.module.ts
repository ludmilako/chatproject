import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { LoginPageComponent } from './login-page/login-page.component';
import {RouterModule, Routes} from "@angular/router";
import {AF} from "../providers/af";
import { HomePageComponent } from './home-page/home-page.component';
import {FormsModule} from "@angular/forms";
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { UsersComponent } from './users/users.component';
import { RoomsComponent } from './rooms/rooms.component';

export const firebaseConfig = {
    apiKey: "AIzaSyB98wRHMDSN9oug0lI3ZJQoQPw_Zmsh3GU",
    authDomain: "chatapp-62fa7.firebaseapp.com",
    databaseURL: "https://chatapp-62fa7.firebaseio.com",
    projectId: "chatapp-62fa7",
    storageBucket: "chatapp-62fa7.appspot.com",
    messagingSenderId: "823635015443"
};

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegistrationPageComponent}
];

@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes),
    FormsModule
  ],
  declarations: [ AppComponent, LoginPageComponent, HomePageComponent, RegistrationPageComponent, UsersComponent, RoomsComponent ],
  bootstrap: [ AppComponent ],
  providers: [AF]
})
export class AppModule {}
