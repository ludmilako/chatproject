import {Component, OnInit, AfterViewChecked, ElementRef, ViewChild} from '@angular/core';
import {AF} from "../../providers/af";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable} from "angularfire2";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  public users: FirebaseListObservable<any>;
  public registeredUsers: FirebaseListObservable<any>;
  public displayName: string;
  public name: string;

  constructor(public af: AngularFire) {
    this.users = this.af.database.list('users');
    this.registeredUsers = this.af.database.list('registeredUsers');
  }

  userSend(theirUser: string){
    this.users.push({user:theirUser});
  }

  registeredUserSend(theirRegisteredUser: string){
    this.users.push({registeredUser:theirRegisteredUser});
  }
}

