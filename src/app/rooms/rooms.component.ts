import {Component, OnInit, AfterViewChecked, ElementRef, ViewChild} from '@angular/core';
import {AF} from "../../providers/af";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable} from "angularfire2";
import { Router } from "@angular/router";

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  public rooms: FirebaseListObservable<any>;
  public roomName: string;
  public newRoom: string;
  public error: any;

  constructor(public af: AngularFire, private afService: AF, private router: Router) {
    this.rooms = this.af.database.list('rooms');
   }

   roomSend(theirRoom: string){
    this.rooms.push({room:theirRoom});
  }


  register(PropertyKey, roomName, email) {
    event.preventDefault();
      this.afService.saveRoomInfoFromForm(PropertyKey, roomName, email).then(() => {
        this.router.navigate(['']);
      })
        .catch((error) => {
          this.error = error;
        });
    
  } 

}



